# i3wm Desktop Ansible Role

Role created to configure a minimal openSUSE Leap desktop using i3wm.

## Requirements

Install openSUSE using the [Server role][1] option. You cas use
Generic Desktop also, but everything from now on is based on Server role.

[At the end of the installation][2], do the following:

1. Change the network management from wicked to Network Manager
2. Click _Software_ and in the [next screen][3], _Details_ to go to [Yast Software Manager][4].
3. Deactivate _Dependencies > Install Recommended Packages_ in the YaST Software Manager main menu.
4. Enable YaST to also remove any other packages that become unneeded after removal of  some specific package, selecting _Options > Cleanup when deleting packages_ from the main menu.

Minimal installations don't install recommended packages (only required ones). Search and check for installation the following packages:

* sudo
* system-group-wheel
* vim
* NetworkManager
* os-prober
* kernel-firmware
* ca-certificates-mozilla

## Configuring and running this ansible role

### Enable `wheel` group to run `sudo`

```bash
su -
visudo
```

Uncomment the following line:

```bash
%wheel ALL=(ALL) ALL
```

Also, comment these two lines:

```bash
#Defaults targetpw   # ask for the password of the target user i.e. root
#ALL   ALL=(ALL) ALL   # WARNING! Only use this together with 'Defaults targetpw'!
```

Save your changes, and run the following commands:

```bash
usermod -aG wheel <your user>
```

Logout and login again.

### Install required packages

```bash
sudo zypper in --no-recommends -y git python3-pip python-xml python2-pyOpenSSL bash-completion
```

### Install ansible

```bash
pip3 install --user ansible
```

Make sure to configure your ansible to point out to the directory where you will have your ansible roles (ex: `/my/roles/directory`).

With everything in place, clone this repository, create a playbook and run it:

```bash
git clone https://gitlab.com/giulianideon/i3wm-desktop-ansible-role.git /my/roles/directory/i3wm-desktop
```

### Role Variables

TBD

## Example Playbook

```yaml
- name: Setup linux desktop
  hosts: localhost
  connection: local
  roles:
    - i3wm-desktop
```

## License

BSD

## Author Information

Giuliani Deon Sanches

[1]: https://doc.opensuse.org/documentation/leap/startup/single-html/book.opensuse.startup/index.html#sec.yast_install.conf.manual.role
[2]: https://doc.opensuse.org/documentation/leap/startup/single-html/book.opensuse.startup/index.html#sec.yast_install.proposal
[3]: https://doc.opensuse.org/documentation/leap/startup/single-html/book.opensuse.startup/index.html#sec.yast_install.proposal.sofware
[4]: https://doc.opensuse.org/documentation/leap/startup/single-html/book.opensuse.startup/index.html#sec.yast-software.qt
